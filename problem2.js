const fs = require('fs');

function readGivenTxtFile (fileName) {
    fs.readFile(fileName, "utf-8", (err, data) => {
        if (err) {
            console.error(`Error: ${err}`);
        } else {
            console.log("Read Data Successfully");
            // console.log(data);
        }
    })
}


function appendNewFileNameToFilenames(textFile, newFileName){
    fs.appendFile(textFile, `${newFileName},`, (err) => {
        if (err) {
            console.error(`Error:  ${err}`);
        }
         else {
            console.log(`${newFileName} file name is added to filenames.txt`);
        }
    })
}

function createNewFileFunction(newFileName, caseData){
    fs.writeFile(newFileName, caseData, (err) => {
        if (err) {
            console.error(`Error: ${err}`);
        } else {
            console.log(`${newFileName} is created successfully.`);
            appendNewFileNameToFilenames("filenames.txt", newFileName)
        }
    })
}


function convertDataToUppercase(fileName, newFileName){
    fs.readFile(fileName, "utf-8", (err, data) => {
        if (err) {
            console.error(`Error: ${err}`);
        } else {
            let upperCaseData = data.toUpperCase();
            // console.log(upperCaseData);
            console.log("New file data converted to upper case");
            createNewFileFunction(newFileName, upperCaseData)
            
        }
    })
}



function convertToLowerCaseAndSplitSentences(fileName, newFileName){
    fs.readFile(fileName, "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            let splittedContent = data.toLowerCase().split(".");
            // console.log(splittedContent);
            for (let i=0; i<splittedContent.length; i++){
                fs.writeFile(newFileName, `${splittedContent[i]}`, (err)=> {
                    if (err){
                        console.error(err);
                    } else {
                        fs.appendFile(newFileName, `${splittedContent[i]}\n`, (err)=>{
                            if(err){
                                console.error(err)
                            }
                            // console.log(`${newFileName} is created successfully`)
                        })
                    }
                })
            }
            fs.appendFile("filenames.txt", `${newFileName},`, (err) => {
                if(err) {
                    console.error(err)
                } else {
                    console.log(`${newFileName} file name is added to filenames.txt`)
                }
            })
        }
    })
}

function readFilesAndSortContent(file1, file2, newFileName){
    fs.readFile(file1, "utf8", (err1, data1) => {
        fs.readFile(file2, "utf8", (err2, data2) => {
            if (err1 || err2) {
                throw new Error();
            }
            else {
                let data = `${data1} \n` + `\n ${data2}`;
                data = data.split(" ")
                data = data.sort()
                for (let i=0; i<data.length; i++){
                    fs.writeFile(newFileName, `${data[i]}`, (err)=> {
                        if (err){
                            console.error(err);
                        } else {
                            fs.appendFile(newFileName, `${data[i]} \n`, (err)=>{
                                if(err){
                                    console.error(err)
                                }
                            })
                        }
                    })
                }
                fs.appendFile("filenames.txt", `${newFileName},`, (err) => {
                    if(err) {
                        console.error(err)
                    } else {
                        console.log(`${newFileName} file name is added to filenames.txt`)
                    }
                })
            }
        })
    })
}


function readAndDeleteFileContent(fileName){
    fs.readFile(fileName, "utf8", (err, data) => {
        if (err) {
            console.error(err)
        } else {
            // console.log(data)
            let files = data.split(",")
            // console.log(files)
            let i = files.length;
            files.forEach( filePath => {
                if (filePath != ""){
                    fs.unlink(filePath, (err) => {
                        i--;
                        if (err) {
                            console.error(err);
                        } 
                        else {
                            console.log("all files removed");
                        }
                    })
                }
                
            });
        }
    })
}


module.exports= {readGivenTxtFile, convertDataToUppercase, convertToLowerCaseAndSplitSentences, readFilesAndSortContent, readAndDeleteFileContent}