const fs = require('fs');

function createRandomJSONFiles(fileName) {
    fs.mkdir(fileName, (err) => {
        if (err) {
            console.error("Error: " +err);
        } else {
            console.log('Folder created Successfully');
        }
    })
}

function createJsonFile (fileName, data) {
    fs.writeFile(fileName, data, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("JSON File created Successfully");
        }
    })
}

function deleteJsonFile (fileName) {
    fs.unlink(fileName, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("JSON File Deleted");
        }
    })
}

module.exports = {createRandomJSONFiles, createJsonFile, deleteJsonFile};