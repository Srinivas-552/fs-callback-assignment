const problem2Module = require('../problem2');

problem2Module.readGivenTxtFile("lipsum.txt")

problem2Module.convertDataToUppercase("lipsum.txt", "newFile1.txt")

problem2Module.convertToLowerCaseAndSplitSentences("newFile1.txt", "newFile2.txt")

problem2Module.readFilesAndSortContent("newFile1.txt", "newFile2.txt", "newFile3.txt")

problem2Module.readAndDeleteFileContent("filenames.txt")







// output:

// amma@vaasu-vostro-3558:~/Desktop/JS_Assignments/Callbacks$ node test/testProblem2.js
// Read Data Successfully
// New file data converted to upper case
// newFile1.txt is created successfully.
// newFile2.txt file name is added to filenames.txt
// newFile1.txt file name is added to filenames.txt
// newFile3.txt file name is added to filenames.txt
// amma@vaasu-vostro-3558:~/Desktop/JS_Assignments/Callbacks$ node test/testProblem2.js
// all files removed
// all files removed
// all files removed
// amma@vaasu-vostro-3558:~/Desktop/JS_Assignments/Callbacks$ 