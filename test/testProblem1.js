// const fs = require('fs');

const callbackModule = require('../problem1');

callbackModule.createRandomJSONFiles('JSONFiles')

callbackModule.createJsonFile('./JSONFiles/file1.json', "JSON File 1")
callbackModule.deleteJsonFile('./JSONFiles/file1.json')

callbackModule.createJsonFile('./JSONFiles/file2.json', "JSON File 2")
callbackModule.deleteJsonFile('./JSONFiles/file2.json')

callbackModule.createJsonFile('./JSONFiles/file3.json', "JSON File 3")
callbackModule.deleteJsonFile('./JSONFiles/file3.json')

callbackModule.createJsonFile('./JSONFiles/file4.json', "JSON File 4")
callbackModule.deleteJsonFile('./JSONFiles/file4.json')
